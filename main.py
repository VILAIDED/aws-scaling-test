from fastapi import FastAPI
import json

app = FastAPI()

@app.get("/")
def read_data():
    with open('data.json', 'r') as json_file:
        data = json.load(json_file)
    return data